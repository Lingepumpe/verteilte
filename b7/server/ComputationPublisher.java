package server;

import javax.xml.ws.Endpoint;

public class ComputationPublisher {
	
	public static void main(String[] args){
		Endpoint.publish("http://localhost:9999/vs/computation", new ComputationImpl());
	}
}
