package server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.math.*;

@WebService
@SOAPBinding(style = Style.RPC)
public interface Computation {

	@WebMethod double add(double a, double b);
	@WebMethod double sub(double a, double b);
	@WebMethod double mul(double a, double b);
	@WebMethod BigInteger fac(long n);
}
