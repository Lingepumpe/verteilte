package server;

import java.math.BigInteger;

import javax.jws.WebService;

@WebService(endpointInterface = "server.Computation")
public class ComputationImpl implements Computation{

	@Override
	public double add(double a, double b) {
		return a+b;
	}

	@Override
	public double sub(double a, double b) {
		return a-b;
	}

	@Override
	public double mul(double a, double b) {
		return a*b;
	}

	@Override
	public BigInteger fac(long n) {
		return factorial(n);
	}
	

    static BigInteger recfact(long start, long n) {
        long i;
        if (n <= 16) { 
            BigInteger r = BigInteger.valueOf(start);
            for (i = start + 1; i < start + n; i++) {
                r = r.multiply(BigInteger.valueOf(i));
            }
            return r;
        }
        i = n / 2;
        return recfact(start, i).multiply(recfact(start + i, n - i));
    }

    static BigInteger factorial(long n) {
        return recfact(1, n);
    }

}
