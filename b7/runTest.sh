#!/bin/bash

set -x

pkill -f java\ server/ComputationPublisher

sleep 2

#run webserver
java server/ComputationPublisher &

sleep 2

#run client
java client/ComputationClient
