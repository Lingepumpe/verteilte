package client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import server.Computation;

public class ComputationClient implements Runnable{

	private String name;
	
	public ComputationClient(String name){
		this.name = name;
	}
	
	public static void main(String[] args) throws Exception{
		Thread client1 = new Thread(new ComputationClient("Client1"));
		Thread client2 = new Thread(new ComputationClient("Client2"));
		Thread client3 = new Thread(new ComputationClient("Client3"));
		Thread client4 = new Thread(new ComputationClient("Client4"));
		
		client1.start();
		client2.start();
		client3.start();
		client4.start();
		
		client1.join();
		client2.join();
		client3.join();
		client4.join();

		System.out.println("--------Starting with benchmark test----------");

		ComputationClient client5 = new ComputationClient("Client5");
		client5.benchmark();
		
	}

	public void benchmark(){
		URL url = null;
		try {
			url = new URL("http://localhost:9999/vs/computation?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.err.println("URL ERROR");
		}
		
		QName qname = new QName("http://server/", "ComputationImplService");
		
		Service service = Service.create(url, qname);
		
		Computation comp = service.getPort(Computation.class);
		
		long time=System.currentTimeMillis(); 
		int counter=0;
		
		for(int i=0; i<10; i++){
			while(System.currentTimeMillis()<time+1000){
				comp.add(60, 50);
				counter++;
			}
			time = System.currentTimeMillis();
		}
		System.out.println("Computed add function "+counter/10 +" times within 1 second.");
	}
	
	@Override
	public void run() {
		Random rand = new Random();
		URL url = null;
		try {
			url = new URL("http://localhost:9999/vs/computation?wsdl");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.err.println("URL ERROR");
		}
		
		QName qname = new QName("http://server/", "ComputationImplService");
		
		Service service = Service.create(url, qname);
		
		Computation comp = service.getPort(Computation.class);
		
		System.out.println(name + " Test add function with parameters 15 and 20, result: " + comp.add(15, 20));
		System.out.println(name + " Test sub function with parameters 15 and 20, result: " + comp.sub(15, 20));
		System.out.println(name + " Test mul function with parameters 15 and 20, result: " + comp.mul(15, 20));
		System.out.println(name + " Test factorial function with parameter 150, result: " + comp.fac(150));
		
		while(true){
			int op = rand.nextInt(4);
			int param1 = rand.nextInt((Integer.MAX_VALUE/2)-1);
			int param2 = rand.nextInt((Integer.MAX_VALUE/2)-1);
			int param3 = rand.nextInt(500);
			switch(op){
			case 0:
				System.out.println(name + " Addition of " + param1 + " and " + param2 + " is " + comp.add(param1, param2));
				break;
			case 1:
				System.out.println(name + " Subtraction of " + param1 + " and " + param2 + " is " + comp.sub(param1, param2));
				break;
			case 2:
				System.out.println(name + " Multiplication of " + param1 + " and " + param2 + " is " + comp.mul(param1, param2));
				break;
			case 3:
				System.out.println(name + " Factorial of " + param3 + " is " + comp.fac(param3));
				break;
			}
			
			if(rand.nextInt(100) == 0)
				break;
			
		}
		
	}
	
}
