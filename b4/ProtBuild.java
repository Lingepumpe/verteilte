import java.util.ArrayList;
import java.util.List;


public class ProtBuild {

	public static String myNodeTable(List<String> table, String myAddress){
		String result="";
		for(String node: table){
			result+=node;
			result+=" ";
		}
		result+=myAddress;
		return "myNodeTable "+result;
	}
	
	public static String oneToAllMessage(int hops, String message){
		return "o2a "+ hops+" "+message;
	}
	
	public static String lookUpName(int hops, String name, String myAddress){
		return "lu "+hops+" "+name+" "+myAddress;
	}

	public static String lookUpNameAnswer(String name, String requestedAddress){
		return "lua " +name+" "+requestedAddress;
	}
	
	public static void test(){
		
		List<String> list=new ArrayList<>();
		list.add("127.0.0.1:1255");
		list.add("192.168.0.8:6687");
		assert(myNodeTable(list, "192.168.0.2:1234").equals("myNodeTable 127.0.0.1:1255 192.168.0.8:6687 192.168.0.2:1234"));
		assert(oneToAllMessage(10,"I am here").equals("o2a 10 I am here"));
		assert(lookUpName(10, "Babylon", "192.168.0.2:1254").equals("lu 10 Babylon 192.168.0.2:1254"));
		assert(lookUpNameAnswer("Babylon","192.168.0.1").equals("lua Babylon 192.168.0.1"));
	}
	
	public static void main(String[] args){
		test();
	}
}
