import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.ServerSocket;
import java.net.SocketException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public class OurP2P {

	private int n;
	private String nodeName;
	private P2PEventListener el=null;
	private NodeTableManagement nodeTableManagement=null;
	private Thread requestThread=null;
	private Thread listenerThread=null;
	private final String ipAddress = "127.0.0.1";
	private final int port;
	private final int hops=2;
	private volatile boolean listening=true;
	private volatile boolean lookUpAnswerReceived=false;
	private Random rnd=new Random();
	
	public OurP2P(String nodeName, int n, P2PEventListener el){
		this.nodeName=nodeName;
		this.n=n;
		this.el=el;
		port=rnd.nextInt(50000)+1024;
	}
	
	
	public void connectToNetwork(String initialNode){
		el.onConnected();
		nodeTableManagement=new NodeTableManagement(buildAddress(ipAddress, port), n);

		/*listener thread accepts requests from other peers*/
		listenerThread= (new Thread(new ListenerThread()));
		listenerThread.start();
		
	/*	try{ 	//Wait  so that nodetable and serversocket is initialized
			Thread.sleep(50);
		}catch (InterruptedException e){
			disconnectFromNetwork();
		}*/
		
		/*start request thread which regularly (every 5 seconds) updates the node table */
		requestThread=(new Thread(new RequestThread(initialNode)));
		requestThread.start();
		
	}
	
	public String getIpAddressAsString(String node){
		return node.split(":")[0];
	}
	
	public int getPortAsInt(String node){
		return Integer.parseInt(node.split(":")[1]);
	}
	
	public String buildAddress(String ip, int port){
		return ip+":"+port;
	}
	
	public void disconnectFromNetwork(){
		listening=false;
		el.onDisconnected();
	}
	
	public boolean isConnected(){
		return listening;
	}
	
	public String getName(){
		return this.nodeName;
	}
	
	public void printStatus(){
		System.out.println("I am node "+nodeName+" and this is my nodeTable: "+nodeTableManagement.getMyNodeTable());
	}
	
	public String getOurAddress(){
		return buildAddress(ipAddress, port);
	}
	
	public void sentToAll(String msg){
		broadCast(ProtBuild.oneToAllMessage(hops, msg));
	}
	
	public void lookUpName(String name){
		broadCast(ProtBuild.lookUpName(hops, name, buildAddress(ipAddress, port)));
	}
	
	private synchronized void broadCast(String msg){
		Socket broadCastSocket=null;
		
		if(lookUpAnswerReceived) lookUpAnswerReceived=false;
	
		
		for(int i=0; i<nodeTableManagement.getMyNodeTable().size();i++){
			String node= nodeTableManagement.getMyNodeTable().get(i);
			try {
				broadCastSocket=new Socket(getIpAddressAsString(node), getPortAsInt(node));
				PrintWriter out = new PrintWriter(broadCastSocket.getOutputStream(), true);
				out.println(msg);
				out.close();
			}catch(ConnectException e){
				nodeTableManagement.removeNonAvailablePeer(node);
			} catch (UnknownHostException e) {
				//ignore
			} catch (IOException e) {
				//ignore
			}finally{
				
				try {
					broadCastSocket.close();
				} catch (Exception e) {
					//ignore
				}
			}
		}
		
		 /* when lookUp is sent client waits 5 seconds for LookUpAnswer, if no answer was received then client is notified*/
		  if(ProtParse.getCommandAsString(msg).equals("lu")){
			  final String tmpMessage=msg;
			  new Thread(new Runnable(){
						  @Override
						  public void run() {
							    try{
								Thread.sleep(5000);
								  if(!lookUpAnswerReceived){
								      
									el.onError("LookUp Failed: "+ProtParse.luGetLookUpNameAsString(tmpMessage)+" did not respond");
								  }else lookUpAnswerReceived=false;
							    }catch (InterruptedException e){
								  //ignore
							    }
						  }
				    }
			  ).start();
		  }
		try{
			Thread.sleep(5000);
		}catch (InterruptedException e){
			disconnectFromNetwork();
		}
		
	
	}
	
	private class ListenerThread implements Runnable{
		
		private ServerSocket p2pServerSocket=null;
		public final int threadPoolSize = 10;
		private ExecutorService service;   

		@Override
		public void run() {
			
	        service = Executors.newFixedThreadPool(threadPoolSize);

		try{
			p2pServerSocket=new ServerSocket(port);
			p2pServerSocket.setSoTimeout(1000);
			while(listening){
				try{
					service.execute(new RequestHandler(p2pServerSocket.accept()));
				}catch(SocketTimeoutException e){
					//check listening variable
				}catch(OutOfMemoryError e){
					System.err.println("Unable to create new native Thread");
				}catch(SocketException e){
					System.err.println("Too many open files");
				}catch(Exception e){
					 e.printStackTrace();
				}
			}
				
		}catch(Exception e) {
		      e.printStackTrace();
	        } finally {
	            try {
	                p2pServerSocket.close();
	            } catch(Exception e) {
			 e.printStackTrace();			
	            }
	            service.shutdown();
	        }

		}
		
	}
	
	private class RequestHandler implements Runnable{

		private Socket clientSocket=null;
			
		public RequestHandler(Socket clientSocket) {
			this.clientSocket=clientSocket;
		}
		
		@Override
		public void run() {
				String received;
				try(PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));){
					received=in.readLine();
					processMessage(received,out);
				}catch(Exception e){				
				}finally{
					try {
						clientSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
		}
		
		public void processMessage(String msg, PrintWriter out){
			if(ProtParse.getCommandAsString(msg).equals("lu")){
				if(nodeName.equals(ProtParse.luGetLookUpNameAsString(msg))){
				
					try {
						
						clientSocket=new Socket(getIpAddressAsString(ProtParse.luGetAddressAsString(msg)),getPortAsInt(ProtParse.luGetAddressAsString(msg)));
						out=new PrintWriter(clientSocket.getOutputStream(), true);
						out.println(ProtBuild.lookUpNameAnswer(nodeName, buildAddress(ipAddress, port)));
					
					}catch (UnknownHostException e) {
						//ignore
					} catch (IOException e) {
						//ignore
					}finally{
						   try {
							clientSocket.close();
							out.close();
						   } catch (Exception e) {
							 //ignore
						   } 
					}
					
				}else if(ProtParse.getHopsAsInt(msg)>0){
					
					int decHops=ProtParse.getHopsAsInt(msg)-1;
					sendToAllNodes(ProtBuild.lookUpName(decHops, ProtParse.luGetLookUpNameAsString(msg),ProtParse.luGetAddressAsString(msg)),out);
				}
				
			}else if(ProtParse.getCommandAsString(msg).equals("o2a")){
				
				el.onToAllMessageReceived(ProtParse.getOneToAllMessageAsString(msg));
				
				if(ProtParse.getHopsAsInt(msg)>0){
				
					int decHops=ProtParse.getHopsAsInt(msg)-1;
					sendToAllNodes(ProtBuild.oneToAllMessage(decHops, ProtParse.getOneToAllMessageAsString(msg)),out);
				
				}
			}else if(ProtParse.getCommandAsString(msg).equals("lua")){
				lookUpAnswerReceived=true;
				el.onNameLookedUp(ProtParse.luaGetLookUpNameAsString(msg), ProtParse.luaGetAddressAsString(msg));
			
			}else if(ProtParse.getCommandAsString(msg).equals("myNodeTable")){
			
				out.println(ProtBuild.myNodeTable(nodeTableManagement.getMyNodeTable(),buildAddress(ipAddress, port)));
				nodeTableManagement.merge(ProtParse.getNodeTableAsList(msg));
				printStatus();
			
			}
		}
		
		public synchronized void sendToAllNodes(String msg, PrintWriter out){
			for(int i=0; i<nodeTableManagement.getMyNodeTable().size();i++){
				String node=nodeTableManagement.getMyNodeTable().get(i);
				try {
					clientSocket=new Socket(getIpAddressAsString(node),getPortAsInt(node));
					out = new PrintWriter(clientSocket.getOutputStream(), true);
					out.println(msg);
					
				}catch(ConnectException e){
					nodeTableManagement.removeNonAvailablePeer(node);
				} catch (UnknownHostException e) {
					//ignore
				} catch (IOException e) {
					//ignore
				}finally{
				    try {
					    clientSocket.close();
					    out.close();
				    } catch (Exception e) {
					    //ignore
				    }
				}
			}
		}
		
	}
	

	private class RequestThread implements Runnable{
	
	private Socket connectionSocket=null;
	private String initialNode;
	
	public RequestThread(String initialNode) {
		this.initialNode=initialNode;
	}
	
	@Override
	public void run() {
		
		String received;
		
		if(!initialNode.equals("")){
			nodeTableManagement.addPeer((initialNode));
		
			try {
				connectionSocket=new Socket(getIpAddressAsString(initialNode),getPortAsInt(initialNode));
		
				try(PrintWriter out = new PrintWriter(connectionSocket.getOutputStream(), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));){
					connectionSocket.setSoTimeout(5000);
					out.println(ProtBuild.myNodeTable(nodeTableManagement.getMyNodeTable(), buildAddress(ipAddress, port)));
					received=in.readLine();
					if(received!=null) nodeTableManagement.merge(ProtParse.getNodeTableAsList(received));
					
				}catch(ConnectException e){
					nodeTableManagement.removeNonAvailablePeer(initialNode);
				}catch (SocketTimeoutException e2){	//peer didn't respond, thus delete node from list
					System.out.println("TimeOut: Initial Node does not respond");
					nodeTableManagement.removeNonAvailablePeer(initialNode);
				}
				
			} catch (UnknownHostException e) {
				//ignore
			} catch (IOException e) {
				//ignore
			}finally{
				try {
					connectionSocket.close();
				}catch (Exception e){
					//ignore
				}
			}
		}
		while(listening){
			if(nodeTableManagement.getMyNodeTable().size()>0){
				int number= rnd.nextInt(nodeTableManagement.getMyNodeTable().size());
				String node=nodeTableManagement.getMyNodeTable().get(number);
				try {
					
					connectionSocket=new Socket(getIpAddressAsString(node),getPortAsInt(node));
					
					try(PrintWriter out = new PrintWriter(connectionSocket.getOutputStream(), true);
							BufferedReader in = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));){
					
						connectionSocket.setSoTimeout(5000);
						out.println(ProtBuild.myNodeTable(nodeTableManagement.getMyNodeTable(), buildAddress(ipAddress, port)));
						nodeTableManagement.merge(ProtParse.getNodeTableAsList(in.readLine()));
						Thread.sleep(5000);
						
					}catch(SocketTimeoutException e){ //node did not respond
						
						nodeTableManagement.removeNonAvailablePeer(nodeTableManagement.getMyNodeTable().get(number));
					
					}catch(ConnectException e){
							nodeTableManagement.removeNonAvailablePeer(node);
					}catch (Exception e){
						//ignore
					}
				}catch(Exception e){
					//ignore
				}finally{
					try {
						connectionSocket.close();
					} catch (Exception e) {
						//ignore
					}
				}
			}
			
			
		}
	}
		
}

}



class NodeTableManagement{
	
	private List<String> myNodeTable=new ArrayList<String>();
	private String myAddress;
	private int n;
	private Random rnd=new Random();
	
	public NodeTableManagement(String myAddress, int n){
		this.myAddress=myAddress;
		this.n=n;
	}
	
	
	public synchronized void merge(List<String> receivedList){
		removeRedundantEntries(receivedList);
		removeOwnAddressFromList(receivedList);
		this.myNodeTable.addAll(receivedList);
		chooseNewTableRandomly();
	}
	
	public  void chooseNewTableRandomly(){
		if(myNodeTable.size()>n){	//if there are more entries than predefined size then choose randomly
			List<String> nList=new ArrayList<String>();
			for(int i=0; i<n; i++){
					int number=rnd.nextInt(myNodeTable.size());
					nList.add(myNodeTable.get(number));
					myNodeTable.remove(number);
			}
			myNodeTable=nList;
		}
	}
	
	public void removeRedundantEntries(List<String> receivedList){
		Iterator<String> it=receivedList.iterator();
		while(it.hasNext()){
			String node=(String) it.next();
			for(int i=0; i<myNodeTable.size();i++){
				if(node.equals(myNodeTable.get(i))) it.remove();
			}
		}
	}
	
	public void removeOwnAddressFromList(List<String> receivedList){
		Iterator<String> it=receivedList.iterator();
		while(it.hasNext()){
			String node=(String) it.next();
			if(node.equals(myAddress)) it.remove();
		}
	}
	
	
	public synchronized void removeNonAvailablePeer(String node){
		Iterator<String> it= myNodeTable.iterator();
		while(it.hasNext()){
			String tmpNode=(String) it.next();
			if(node.equals(tmpNode)) it.remove();
		}
	}
	
	public synchronized void addPeer(String node){
		myNodeTable.add(node);
	}
	
	public synchronized List<String> getMyNodeTable(){
		return this.myNodeTable;
	}
	
}
