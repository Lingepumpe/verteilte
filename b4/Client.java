import java.util.Random;


public class Client implements P2PEventListener {

	
	public static void main(String[] args){
		if(args.length==2){

			final Client[] clients= new Client[3*Integer.parseInt(args[0])];
			
			try {
			      Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				
					@Override
					public void run() {
						for(Client a: clients){
							if(a.op2p.isConnected()) a.op2p.disconnectFromNetwork();
						}
					}
				}));
			    } catch (Throwable t) {
			      System.out.println("[Main thread] Could not add Shutdown hook");
			}
	        
			if(args[1].toLowerCase().equals("test1")) test1(clients,Integer.parseInt(args[0]));
			else if (args[1].toLowerCase().equals("test2")) test2(clients,Integer.parseInt(args[0]));			
			else if (args[1].toLowerCase().equals("test3")) test3(clients,Integer.parseInt(args[0]));
			else if (args[1].toLowerCase().equals("test4")) test4(clients,Integer.parseInt(args[0]));
			
		}else{
			System.out.println("Usage: java Client [size of NodeTable] Test[1-4], example: java Client 4 test1");
			System.out.println("Test1: check nodetable exchange");
			System.out.println("Test2: check one2All message");
			System.out.println("Test3: check lookUp name");
			System.out.println("Test4: check everything");
		}
	}
	
	
	private OurP2P op2p=null;
	private String iAm;

	
	public Client(String name, int n,String connectTo){
		this.iAm=name;
		op2p= new OurP2P(name, n, this);
		op2p.connectToNetwork(connectTo);
	}
	
	public static void test1(Client[] clients, int n){
		Random rnd=new Random();
		
		for(int i=0; i<clients.length; i++){
			if(i==0){
				clients[i]=new Client("Peer"+i,n,"");
			}else{
				clients[i]=new Client("Peer"+i,n,clients[0].op2p.getOurAddress());
			}
		}
		
		while(true){
			
			int nextClient=rnd.nextInt(clients.length);
			Client tmp=clients[nextClient];
			if(rnd.nextInt(5)==0){
						tmp.op2p.disconnectFromNetwork();
						Client[] tmpClient=new Client[clients.length-1];
						for(int i=0,j=0; i<clients.length; i++){
							if(i==nextClient)continue;
							else tmpClient[j++]=clients[i];
						}
						clients=tmpClient;
			}
			
			boolean terminate=true;
			for(Client a: clients){
				if(a.op2p.isConnected()) terminate=false;
			}
			if(terminate) break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public static void test2(Client[] clients, int n){
		Random rnd=new Random();
		
		for(int i=0; i<clients.length; i++){
			if(i==0){
				clients[i]=new Client("Peer"+i,n,"");
			}else{
				clients[i]=new Client("Peer"+i,n,clients[0].op2p.getOurAddress());
			}
		}
		
		while(true){
			int nextClient=rnd.nextInt(clients.length);
			Client tmp=clients[nextClient];
			int task=rnd.nextInt(2);
			switch(task){
				case 0:
					switch(rnd.nextInt(3)){
						case 0:
							tmp.op2p.sentToAll("It's impossible to describe the warmth you feel when witnessing the rise of the sun");
							break;
						case 1:
							tmp.op2p.sentToAll("Raise your hand when your asked if anybody can help an orphan child");
							break;
						case 2:
							tmp.op2p.sentToAll("With great power comes great responsibility");
							break;
					}
					break;
				case 1:
					if(rnd.nextInt(5)==0){
						tmp.op2p.disconnectFromNetwork();
						Client[] tmpClient=new Client[clients.length-1];
						for(int i=0,j=0; i<clients.length; i++){
							if(i==nextClient)continue;
							else tmpClient[j++]=clients[i];
						}
						clients=tmpClient;
					}
					break;
			}
			
			
			boolean terminate=true;
			for(Client a: clients){
				if(a.op2p.isConnected()) terminate=false;
			}
			if(terminate) break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	public static void test3(Client[] clients, int n){
		Random rnd=new Random();
		
		for(int i=0; i<clients.length; i++){
			if(i==0){
				clients[i]=new Client("Peer"+i,n,"");
			}else{
				clients[i]=new Client("Peer"+i,n,clients[0].op2p.getOurAddress());
			}
		}
		
		while(true){
			
			int nextClient=rnd.nextInt(clients.length);
			Client tmp=clients[nextClient];
			int task=rnd.nextInt(2);
			switch(task){
				case 0:
					int choose=rnd.nextInt(clients.length);
					if(!tmp.op2p.getName().equals(clients[choose].op2p.getName()))
						tmp.op2p.lookUpName(clients[choose].op2p.getName());
					break;
				case 1:
					if(rnd.nextInt(5)==0){
						tmp.op2p.disconnectFromNetwork();
						Client[] tmpClient=new Client[clients.length-1];
						for(int i=0,j=0; i<clients.length; i++){
							if(i==nextClient)continue;
							else tmpClient[j++]=clients[i];
						}
						clients=tmpClient;
					}
					break;
			}
			
			boolean terminate=true;
			for(Client a: clients){
				if(a.op2p.isConnected()) terminate=false;
			}
			if(terminate) break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void test4(Client[] clients, int n){
		Random rnd=new Random();
		
		for(int i=0; i<clients.length; i++){
			if(i==0){
				clients[i]=new Client("Peer"+i,n,"");
			}else{
				clients[i]=new Client("Peer"+i,n,clients[0].op2p.getOurAddress());
			}
		}
		
		while(true){
			int nextClient=rnd.nextInt(clients.length);
			Client tmp=clients[nextClient];
			int task=rnd.nextInt(3);
			switch(task){
				case 0:
					switch(rnd.nextInt(3)){
						case 0:
							tmp.op2p.sentToAll("It's impossible to describe the warmth you feel when witnessing the rise of the sun");
							break;
						case 1:
							tmp.op2p.sentToAll("Raise your hand when your asked if anybody can help an orphan child");
							break;
						case 2:
							tmp.op2p.sentToAll("With great power comes great responsibility");
							break;
					}
					break;
				case 1:
					if(!tmp.op2p.getName().equals(clients[rnd.nextInt(clients.length)].op2p.getName()))
						tmp.op2p.lookUpName(clients[rnd.nextInt(clients.length)].op2p.getName());
					break;
				case 2:
					if(rnd.nextInt(5)==0){
						tmp.op2p.disconnectFromNetwork();
						Client[] tmpClient=new Client[clients.length-1];
						for(int i=0,j=0; i<clients.length; i++){
							if(i==nextClient)continue;
							else tmpClient[j++]=clients[i];
						}
						clients=tmpClient;
					}
					break;
			}
			boolean terminate=true;
			for(Client a: clients){
				if(a.op2p.isConnected()) terminate=false;
			}
			if(terminate) break;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onError(String errorMessage) {
		System.out.println("I am " + iAm+": Error message received: "+errorMessage);
	}

	@Override
	public void onConnected() {
		System.out.println("I am " + iAm+": Connected to network");
	}

	@Override
	public void onDisconnected() {
		System.out.println("I am " + iAm+": Disconnected from network");
	}

	@Override
	public void onToAllMessageReceived(String message) {
		System.out.println("I am " + iAm+": OneToAllMessage received: "+message);
	}

	@Override
	public void onNameLookedUp(String name, String result) {
		System.out.println("I am " + iAm+": Peer with the name "+name+ " can be reached by "+result);
	}

}
