import java.util.ArrayList;
import java.util.List;


public class ProtParse {

	public static List<String> getNodeTableAsList(String receivedNodeTable){
		List<String> result=new ArrayList<String>();
		String[] tmpTable=new String[receivedNodeTable.split(" ").length];
		tmpTable=receivedNodeTable.split(" ");
		for(int i=1; i<tmpTable.length;i++){
			result.add(tmpTable[i]);
		}
		return result;
	}
	
	public static String getOneToAllMessageAsString(String msg){
		String[] tmpMsg=msg.split(" ");
		String result="";
		for(int i=2; i<tmpMsg.length; i++){
			result+=tmpMsg[i];
			if(i!=tmpMsg.length-1) result+=" ";
		}
		return result;
	}
	
	public static int getHopsAsInt(String msg){
		return Integer.parseInt(msg.split(" ")[1]);
	}
	
	public static String getHopsAsString(String msg){
		return msg.split(" ")[1];
	}
	
	public static String getCommandAsString(String msg){
		return msg.split(" ")[0];
	}
	
	public static String luGetAddressAsString(String msg){
		return msg.split(" ")[3];
	}
	
	public static String luGetLookUpNameAsString(String msg){
		return msg.split(" ")[2];
	}
	
	public static String luaGetAddressAsString(String msg){
		return msg.split(" ")[2];
	}
	
	
	public static String luaGetLookUpNameAsString(String msg){
		return msg.split(" ")[1];
	}
	
	public static void test(){

		assert(getOneToAllMessageAsString("o2a 10 Hello I am here?").equals("Hello I am here?"));
		assert(getHopsAsString("lu 10 Babylon 192.168.0.1:8888").equals("10"));
		assert(getHopsAsString("o2a 10 Check it out!").equals("10"));
		assert(getHopsAsInt("lu 10 Babylon 192.168.0.1:8888")==10);
		assert(getCommandAsString("o2a 10 Check it out!").equals("o2a"));
		assert(getCommandAsString("lu 10 Babylon 192.168.0.1:8888").equals("lu"));
		assert(getCommandAsString("myNodeTable 192.0.0.2:8888").equals("myNodeTable"));
		assert(luGetAddressAsString("lu 10 Babylon 192.168.0.1:8888").equals("192.168.0.1:8888"));
		assert(luGetLookUpNameAsString("lu 10 Babylon 192.168.0.1:8888").equals("Babylon"));
		assert(luaGetAddressAsString("lua Babylon 192.168.0.1:8888").equals("192.168.0.1:8888"));
		assert(luaGetLookUpNameAsString("lua Babylon 192.168.0.1:8888").equals("Babylon"));
	}
	
	public static void main(String[] args){
		test();
	}
	
}
