
public interface P2PEventListener {

	public void onError(String errorMessage);
	
	public void onConnected();
	
	public void onDisconnected();
	
	public void onToAllMessageReceived(String message);
	
	public void onNameLookedUp(String name, String result);
}
