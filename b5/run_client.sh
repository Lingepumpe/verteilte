#!/bin/bash

set -x
set -e

java -cp .:html/pub/classes/compute.jar \
     -Djava.rmi.server.codebase=file://html/pub/classes \
     -Djava.security.policy=client.policy \
     client.Client

