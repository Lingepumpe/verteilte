package engine;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import compute.Compute;
import compute.Task;
import compute.Listen;
import java.math.BigInteger;

public class ComputeEngine implements Compute {
    long sequenceID = 0;

    public ComputeEngine() {
        super();
    }

    public <T> T executeTask(Task<T> t)  throws RemoteException {
        return t.execute();
    }

    @Override
    public <T> long executeTaskAsync(final Task<T> t, final Listen el)  throws RemoteException {
        final long ourID = getAndIncSequenceCounter();
        new Thread(new Runnable() { //execute task t in a thread, pass in el
                @Override
                public void run() {
                    try {
                        el.givenAnswer(ourID, t.execute()); //once the answer is computed, notify caller via callback
                    } catch(RemoteException e) {
                        e.printStackTrace();
                        System.exit(-1);
                    }
                }
            }).start();

        return ourID;
    }

    public double calcSum(double a, double b)  throws RemoteException {
        return a+b;
    }
    
    public double calcDif(double a, double b)  throws RemoteException {
        return a-b;
    }
    
    public double calcProd(double a, double b)  throws RemoteException {
        return a*b;
    }
    
    public BigInteger calcFac(long n)  throws RemoteException {
        return recfact(1, n);
    }

    private static BigInteger recfact(long start, long n) {
        long i;
        if (n <= 16) { 
            BigInteger r = BigInteger.valueOf(start);
            for (i = start + 1; i < start + n; i++) {
                r = r.multiply(BigInteger.valueOf(i));
            }
            return r;
        }
        i = n / 2;
        return recfact(start, i).multiply(recfact(start + i, n - i));
    }

    private synchronized long getAndIncSequenceCounter() {
        return sequenceID++;
    }


    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            Compute stub =
                (Compute) UnicastRemoteObject.exportObject(engine, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("ComputeEngine bound");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }
}
