#!/bin/bash

set -x
set -e

CWD="`pwd`"
HTTP_PUB="http/pub"

#start webserver
(killall python 2> /dev/null && sleep 2) || true
cd ${HTTP_PUB}
python -m SimpleHTTPServer 8000 &
cd ${CWD}
sleep 2


(killall rmiregistry 2> /dev/null && sleep 3) || true #make sure it is not already running
rmiregistry &
sleep 3 #some time to let the rmiregistery spawn

java -cp .:compute.jar -Djava.rmi.server.codebase=file://compute.jar -Djava.rmi.server.hostname=localhost -Djava.security.policy=server.policy engine.ComputeEngine


