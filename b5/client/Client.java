package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.math.BigDecimal;
import java.math.BigInteger;
import compute.Compute;
import compute.Listen;

public class Client implements Listen {
    int answersReceived = 0;
    Registry registry;

    public Client() {
        super();

        try {
            String cname ="Client";
            Listen stub = (Listen) UnicastRemoteObject.exportObject(this, 0);
            registry = LocateRegistry.getRegistry();
            registry.rebind(cname, stub);
            System.out.println("Listen bound");
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public <T> void givenAnswer(long requestID, T answer) {
        System.out.println("Got an answer for requestID " + requestID + ", it was: " + answer);
        synchronized(this) {
            ++answersReceived;
            if(answersReceived >= 3) {
                System.out.println("Received all answers, shutting down listen");
                try {
                    UnicastRemoteObject.unexportObject(this, true);
                } catch(Exception e) {
                    e.printStackTrace();
                    System.exit(-1);
                }
            }
        }
    }

    public static void main(String args[]) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        Client c = new Client();
        c.runTests();
    }

    private void runTests() {
        try {
            Compute comp = (Compute) registry.lookup("Compute");

            //testing fixed built in functions
            System.out.println("\n=== Testing built in calc functions ===");
            double a = 4;
            double b = 5;
            System.out.println("calcSum(" + a + ", " + b + ") gives: " + comp.calcSum(a, b));
            System.out.println("calcDif(" + a + ", " + b + ") gives: " + comp.calcDif(a, b));
            System.out.println("calcProd(" + a + ", " + b + ") gives: " + comp.calcProd(a, b));
            System.out.println("calcFac(256) gives: " + comp.calcFac(256));

            //calculate "custom" task1
            System.out.println("\n=== Testing executeTask, with task \"Pi\" ===");
            Pi task = new Pi(100);
            BigDecimal pi = comp.executeTask(task);
            System.out.println("Pi(100): " + pi);

            //calculate "custom" task2
            System.out.println("\n=== Testing executeTask, with task \"DeepThought\" ===");
            DeepThought dt = new DeepThought("What is 7*6, Deep Thought?");
            String answer = comp.executeTask(dt); //our tests find: The server can handle multiple paralell requests
            System.out.println("DeepThought has wise words for us: " + answer + ".");

            System.out.println("\n=== Testing executeTaskAsync, with task 3*\"DeepThought\" tasks ===");
            DeepThought dt1 = new DeepThought("Why can't the first monkey hear?");
            DeepThought dt2 = new DeepThought("Why can't the second monkey see?");
            DeepThought dt3 = new DeepThought("Why can't the third monkey speak?");
            System.out.println("comp.executeTaskAsync(dt1, this) accepted with task id " + comp.executeTaskAsync(dt1, this));
            System.out.println("comp.executeTaskAsync(dt2, this) accepted with task id " + comp.executeTaskAsync(dt2, this));
            System.out.println("comp.executeTaskAsync(dt3, this) accepted with task id " + comp.executeTaskAsync(dt3, this));
        } catch (Exception e) {
            System.err.println("calculation exception:");
            e.printStackTrace();
        }
    }    
}
