package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Listen extends Remote {
    <T> void givenAnswer(long requestID, T answer) throws RemoteException;
}
