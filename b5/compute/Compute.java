package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.math.BigInteger;

public interface Compute extends Remote {
    <T> T executeTask(Task<T> t) throws RemoteException;
    <T> long executeTaskAsync(Task<T> t, Listen el) throws RemoteException;
    double calcSum(double a, double b) throws RemoteException;
    double calcDif(double a, double b) throws RemoteException;
    double calcProd(double a, double b) throws RemoteException;
    BigInteger calcFac(long n) throws RemoteException;
}
