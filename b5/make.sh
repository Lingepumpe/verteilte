#!/bin/bash

set -x
set -e

#compute
javac compute/Compute.java compute/Task.java compute/Listen.java
jar cvf compute.jar compute/*.class

#engine
javac -cp compute.jar engine/ComputeEngine.java

#client
javac -cp compute.jar client/Client.java client/Pi.java client/DeepThought.java

#copy to web server directory

HTTP_PUB="http/pub"
mkdir -p ${HTTP_PUB}/classes/client
cp client/Pi.class ${HTTP_PUB}/classes/client
cp client/DeepThought.class ${HTTP_PUB}/classes/client

