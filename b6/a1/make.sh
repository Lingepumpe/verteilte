#!/bin/bash

set -x
set -e

#compute
javac -Xlint:unchecked compute/Compute.java compute/Listen.java
jar cvf compute.jar compute/*.class

#engine
javac -Xlint:unchecked -cp compute.jar engine/ComputeEngine.java

#client
javac -Xlint:unchecked -cp compute.jar client/Client.java client/Pi.java client/DeepThought.java client/JobProcessingService.java client/Job.java

#copy to web server directory

HTTP_PUB="http/pub"
mkdir -p ${HTTP_PUB}/classes/client
cp client/Pi.class ${HTTP_PUB}/classes/client
cp client/DeepThought.class ${HTTP_PUB}/classes/client

