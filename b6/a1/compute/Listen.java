package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Listen<T> extends Remote {
    void givenAnswer(T answer) throws RemoteException;
}
