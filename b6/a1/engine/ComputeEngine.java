package engine;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import compute.Compute;
import compute.Listen;

public class ComputeEngine implements Compute {
    private int m_activeTasks = 0;
    private static final int MAX_TASKS = 2; //limit of how many tasks the compute engine will run in parallel
    private static final int THREADPOOL_THREADS = Math.min(MAX_TASKS, Runtime.getRuntime().availableProcessors());
    private final ExecutorService service = Executors.newFixedThreadPool(THREADPOOL_THREADS);

    @Override
    public <T> boolean executeTaskAsync(final Callable<T> t, final Listen<T> el)  throws RemoteException {
        if(newTaskIfPossible()) {
            service.submit(new Runnable() { //execute task t in a thread, pass in el
                    @Override
                    public void run() {
                        try {
                            el.givenAnswer(t.call()); //once the answer is computed, notify caller via callback
                            taskCompleted();
                        } catch(Exception e) {
                            e.printStackTrace();
                            System.exit(-1);
                        }
                    }
                });
            return true;
        }
        return false;
    }

    private synchronized boolean newTaskIfPossible() {
        if(m_activeTasks >= MAX_TASKS) {
            return false;
        }
        ++m_activeTasks;
        return true;
    }

    private synchronized void taskCompleted() {
        assert(m_activeTasks > 0);
        --m_activeTasks;
    }

    public static void main(String[] args) {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = "Compute";
            Compute engine = new ComputeEngine();
            Compute stub = (Compute) UnicastRemoteObject.exportObject(engine, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);
            System.out.println("ComputeEngine bound");
        } catch (Exception e) {
            System.err.println("ComputeEngine exception:");
            e.printStackTrace();
        }
    }
}
