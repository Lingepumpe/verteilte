package client;

import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.RemoteException;
import compute.Compute;
import compute.Listen;

class JobImpl<T> extends UnicastRemoteObject implements Job<T>, Listen<T> {
    private volatile boolean m_isDone = false;
    private T m_result = null;

    public JobImpl(Compute comp, Callable<T> callable) throws Exception {
        comp.executeTaskAsync(callable, this);
    }
    
    //listen interface
    @Override
    public synchronized void givenAnswer(T answer) {
        m_result = answer;
        m_isDone = true;
    }

    //job interface
    @Override
    public boolean isDone() {
        return m_isDone;
    }
    
    @Override
    public synchronized T getResult() {
        if(m_result == null) {
            throw new IllegalStateException("No result present");
        }
        try {
            UnicastRemoteObject.unexportObject(this, true);
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return m_result;
    }

}

class JobProcessingService {
    private Registry m_registry;
    private Compute m_comp;

    public JobProcessingService() {
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }


        try {
            m_registry = LocateRegistry.getRegistry();
            m_comp = (Compute)m_registry.lookup("Compute");
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
    
    public <T> Job<T> submit(Callable<T> callable) throws RemoteException {
        try {
            Job<T> j = new JobImpl<T>(m_comp, callable);
            return j;
        } catch(Exception e) {
            return null;
        }
    }
}