package client;

import java.util.concurrent.Callable;
import java.io.Serializable;

public class DeepThought implements Callable<String>, Serializable {

    private static final long serialVersionUID = 228L;
    private final String question;

    public DeepThought(String question) {
        this.question = question;
    }

    public String call() {
        try {
            Thread.sleep(3*1000);
        } catch(Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return "The answer to your question \"" + question + "\" is probably 42";
    }
}