package client;

public interface Job<T> {
    boolean isDone();
    T getResult();
}
