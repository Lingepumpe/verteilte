package client;

import java.math.BigDecimal;


public class Client {
    public static void main(String args[]) {
        Client c = new Client();
        c.runTests();
    }

    private void runTests() {
        try {
            JobProcessingService jps = new JobProcessingService();

            //calculate "custom" task1
            System.out.println("\n=== Testing submit, with task \"Pi\" ===");
            Pi pi = new Pi(100);
            Job<BigDecimal> piJob = jps.submit(pi);
            if(piJob != null) {
                while(!piJob.isDone()) { //wait for job to complete...
                    Thread.sleep(10);
                }
                System.out.println("Pi(100): " + piJob.getResult());
            } else {
                System.out.println("Pi job rejected");
            }

            //calculate "custom" task2
            System.out.println("\n=== Testing submit, with task \"DeepThought\" ===");
            DeepThought dt = new DeepThought("What is 7*6, Deep Thought?");
            Job<String> dtJob = jps.submit(dt);
            if(dtJob != null) {
                while(!dtJob.isDone()) { //wait for job to complete...
                    Thread.sleep(10);
                }
                System.out.println("DeepThought has wise words for us: " + dtJob.getResult() + ".");
            } else {
                System.out.println("dt job rejected");
            }

            System.out.println("\n=== Testing paralell submits, with task 3*\"DeepThought\" tasks ===");
            DeepThought dt1 = new DeepThought("Why can't the first monkey hear?");
            DeepThought dt2 = new DeepThought("Why can't the second monkey see?");
            DeepThought dt3 = new DeepThought("Why can't the third monkey speak?");
            Job<String> dtJob1 = jps.submit(dt1);
            Job<String> dtJob2 = jps.submit(dt2);
            Job<String> dtJob3 = jps.submit(dt3);
            while((dtJob3 != null && !dtJob3.isDone()) || (dtJob2 != null && !dtJob2.isDone()) || (dtJob1 != null && !dtJob1.isDone())) { //wait for job to complete...
                Thread.sleep(10);
            }
            System.out.println("All jobs complete, result are: ");
            if(dtJob1 != null) {
                System.out.println(dtJob1.getResult());
            } else {
                System.out.println("dtJob1 rejected");
            }
            if(dtJob2 != null) {
                System.out.println(dtJob2.getResult());
            } else {
                System.out.println("dtJob2 rejected");
            }
            if(dtJob3 != null) {
                System.out.println(dtJob3.getResult());
            } else {
                System.out.println("dtJob3 rejected");
            }
        } catch (Exception e) {
            System.err.println("calculation exception:");
            e.printStackTrace();
        }
    }    
}