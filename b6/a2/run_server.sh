#!/bin/bash

set -x
set -e

java -ea -cp .:compute.jar -Djava.rmi.server.codebase=file://compute.jar -Djava.rmi.server.hostname=localhost -Djava.security.policy=server.policy engine.ComputeEngine


