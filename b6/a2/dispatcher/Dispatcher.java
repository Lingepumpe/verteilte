package dispatcher;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import compute.Compute;
import compute.Listen;
import compute.Register;

public class Dispatcher implements Compute, Register {

	private List<Compute> servers=new ArrayList<Compute>();
	private int serverChooser=0;
	
	@Override
	public <T> boolean executeTaskAsync(Callable<T> t, Listen<T> el)
			throws RemoteException {
		synchronized(this){
		  while(true){
			  if(servers.get(serverChooser).executeTaskAsync(t, el)){
				  break;
			  }
			  serverChooser= (++serverChooser) % servers.size();
		  }
		}
		return true;
	}

	@Override
	public synchronized void register(Compute server) throws RemoteException {
		servers.add(server);
	}
	
	public static void main(String[] args){
		try{
			String name = "Compute";
	        Compute engine = new Dispatcher();
	        Compute stub = (Compute) UnicastRemoteObject.exportObject(engine, 0);
	        Registry registry = LocateRegistry.getRegistry();
	        registry.rebind(name, stub);
	        System.out.println("Dispatcher bound");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

}
