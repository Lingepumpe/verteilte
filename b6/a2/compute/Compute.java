package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.concurrent.Callable;


public interface Compute extends Remote {
    <T> boolean executeTaskAsync(Callable<T> t, Listen<T> el) throws RemoteException;
}