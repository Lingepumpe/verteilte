package compute;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Register extends Remote {
	void register(Compute server) throws RemoteException;
}
