#!/bin/bash

set -x
set -e

CWD="`pwd`"
HTTP_PUB="http/pub"

#start webserver
(killall python 2> /dev/null && sleep 2) || true
cd ${HTTP_PUB}
python -m SimpleHTTPServer 8000 &
cd ${CWD}


(killall rmiregistry 2> /dev/null && sleep 3) || true #make sure it is not already running
rmiregistry &

sleep 3


sh run_dispatcher.sh &

sleep 3

sh run_server.sh &
sh run_server.sh &

sleep 3

clients="Client_One Client_Two Client_Three Client_Four Client_Five"
for client in $clients
do
 echo "$client"
 sh run_client.sh &
done
