#!/bin/bash

set -x
set -e

killall java || true

sleep 1

sh createKeys.sh &

sleep 2

sh runServer.sh &

sleep 2

sh runClient.sh
