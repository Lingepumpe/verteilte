package rsa;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;

import javax.xml.bind.DatatypeConverter;

public class Client {

	private int portNumber = 9191;
	private String hostName = "localhost";
	

	public static void main(String[] args){
		if(args.length!=2){
			System.out.println("First parameter must be filename of client privatekey and second parameter must be filename of public key of server");
		}
		
		String dirPrivateKey=args[0];
		String dirPublicKey=args[1];
		
	//	String dirPrivateKey = "client_private.key";
	//	String dirPublicKey = "server_public.key";
		
		PrivateKey privateKey=null;
		PublicKey publicKey=null;
		try(ObjectInputStream objIn=new ObjectInputStream(new FileInputStream(dirPrivateKey));
			ObjectInputStream	objIn2 = new ObjectInputStream(new FileInputStream(dirPublicKey));	){
			
			privateKey=(PrivateKey) objIn.readObject();
			publicKey=(PublicKey) objIn2.readObject();
			
		}catch(ClassNotFoundException | IOException e){
			e.printStackTrace();
		}
		Client client = new Client();
		client.sendMessage(privateKey, publicKey);

	}
	
	
	public void sendMessage(PrivateKey privateKey, PublicKey publicKey){
		String received;
		try{
			String message;
			Scanner scan=new Scanner(System.in);
			Socket sock= new Socket(hostName, portNumber);
			PrintWriter out= new PrintWriter(sock.getOutputStream(),true);
			try(BufferedReader in= new BufferedReader(new InputStreamReader(sock.getInputStream()))){
				while(true){
					System.out.println("Type in message: ");
					message=scan.nextLine();
					out.println(DatatypeConverter.printHexBinary(RSA.encrypt(message, publicKey)));
					received=in.readLine();
					System.out.println("Client_log Received: "+received);
					System.out.println("Client_log Decrypted: "+RSA.decrypt(DatatypeConverter.parseHexBinary(received), privateKey));
					if(message.equalsIgnoreCase("exit")) break;
					System.out.println("--------------------------------------------------------");
				}
			}
			
		}catch(UnknownHostException e){
			e.printStackTrace();
			System.exit(1);
		}catch(IOException e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	
}
