package rsa;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.bind.DatatypeConverter;

public class Server {

	public static final int listenPort = 9191;
	public static final int threadPoolSize = 10;

	private ExecutorService service;
	boolean listening = true;

	public static void main(String[] args){
		if(args.length != 2){
			System.err.println("ERROR\n Usage: java Server server_private.key server_public.key");
		}
		String dirPrivateKey = args[0];
		String dirPublicKey = args[1];
		
	//	String dirPrivateKey = "server_private.key";
	//	String dirPublicKey = "client_public.key";
		
		PrivateKey privateKey = null;
		PublicKey publicKey = null;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(dirPrivateKey));
		    privateKey = (PrivateKey) in.readObject();
		    
		    in = new ObjectInputStream(new FileInputStream(dirPublicKey));
		    publicKey = (PublicKey) in.readObject();
		} catch (FileNotFoundException e) {
			System.err.println("File no found");
		} catch (IOException e) {
			System.err.println("IOException");
		} catch (ClassNotFoundException e) {
			System.err.println("Class no found");
		}
		
		Server s = new Server();
		s.listenHome(privateKey, publicKey);
		System.out.println("end server");
	}
	
	
	private void listenHome(PrivateKey privateKey, PublicKey publicKey) {
		service = Executors.newFixedThreadPool(threadPoolSize);
		try {
			ServerSocket serverSocket = new ServerSocket(listenPort);
			while (listening) {
				try {
					service.execute(new RequestHandler(serverSocket.accept(), privateKey, publicKey));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("Exception caught");
		} finally {
			service.shutdown();
		}
	}

}

class RequestHandler implements Runnable {
	private final Socket socket;
	private boolean areAuthed = false;
	private PrivateKey privateKey;
	private PublicKey publicKey;

	public RequestHandler(Socket s, PrivateKey privateKey, PublicKey publicKey) {
		socket = s;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	@Override
	public void run() {
		boolean exit = false;
		System.out.println("New Connection");
		String inputLine;
		try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true) ; BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));) {
			while(!exit){
				while ((inputLine = in.readLine()) != null) {
					System.out.println("Server_log Received: " + inputLine);
					String plainText = RSA.decrypt(DatatypeConverter.parseHexBinary(inputLine), privateKey);
					if(plainText.equalsIgnoreCase("exit")) exit = true;
					System.out.println("Server_log Decrypted: " + plainText);
					out.println(DatatypeConverter.printHexBinary(RSA.encrypt("OK", publicKey)));
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Connection closed");
	}
}
