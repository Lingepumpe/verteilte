package hybrid;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Scanner;

import javax.xml.bind.DatatypeConverter;

public class Client {

	private int portNumber = 9191;
	private String hostName = "localhost";
	private final SSSC encryptDecrypt;
	private String key;
	
	public Client(String key){
		byte[] nonce= new byte[8];
		this.key=key;
		encryptDecrypt=new SSSC(DatatypeConverter.parseHexBinary(key), nonce);
	}

	public static void main(String[] args){
		if(args.length!=3){
			System.out.println("First parameter must be privatekey file, second parameter must be publickey file, third parameter has to be a secret key");
		}
		
		String dirPrivateKey=args[0];
		String dirPublicKey=args[1];
		String secretKey=args[2];
		
//		String dirPrivateKey = "client_private.key";
//		String dirPublicKey = "server_public.key";
//		String secretKey="dcafb51ad00497b684b6ada5d0876a9fc4fee977d310c9c12bf254d72bec87c6";
				
		PrivateKey privateKey=null;
		PublicKey publicKey=null;
		try(ObjectInputStream objIn=new ObjectInputStream(new FileInputStream(dirPrivateKey));
			ObjectInputStream	objIn2 = new ObjectInputStream(new FileInputStream(dirPublicKey));	){
			
			privateKey=(PrivateKey) objIn.readObject();
			publicKey=(PublicKey) objIn2.readObject();
			
		}catch(ClassNotFoundException | IOException e){
			e.printStackTrace();
		}
		Client client = new Client(secretKey);
		client.sendMessage(privateKey, publicKey);

	}
	
	
	public void sendMessage(PrivateKey privateKey, PublicKey publicKey){
		boolean keyExchanged=false;
		byte[] plainText;
		byte[] cipherText;
		String received;
		try{
			String message;
			Scanner scan=new Scanner(System.in);
			Socket sock= new Socket(hostName, portNumber);
			PrintWriter out= new PrintWriter(sock.getOutputStream(),true);
			try(BufferedReader in= new BufferedReader(new InputStreamReader(sock.getInputStream()))){
				while(true){
					if(!keyExchanged){
						out.println(DatatypeConverter.printHexBinary(RSA.encrypt(key, publicKey)));
						System.out.println("Client_log Decrypted Received: "+RSA.decrypt(DatatypeConverter.parseHexBinary(in.readLine()), privateKey));
						keyExchanged=true;
						System.out.println("Key successfully sent");
						System.out.println("-------------------------------------------");
					}else{
						System.out.println("Type in message: ");
						message=scan.nextLine();
						plainText = message.getBytes(Charset.forName("UTF-8"));
						cipherText=new byte[plainText.length];
						encryptDecrypt.processBytes(plainText, cipherText);
						out.println(DatatypeConverter.printHexBinary(cipherText));
						
						received=in.readLine();
						cipherText= DatatypeConverter.parseHexBinary(received);
						plainText=new byte[cipherText.length];
						encryptDecrypt.processBytes(cipherText, plainText);
						System.out.println("Client_log Received: "+received);
						System.out.println("Client_log Decrypted: "+new String(plainText,"UTF-8"));
						System.out.println("-------------------------------------------");
						if(message.equalsIgnoreCase("exit")) break;
					}
				}
			}
			
		}catch(UnknownHostException e){
			e.printStackTrace();
			System.exit(1);
		}catch(IOException e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	
}
