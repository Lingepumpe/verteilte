package hybrid;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class RSA {
	
	public static void generateKey(String privateKey, String publicKey){
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			keyGen.initialize(1024);
			KeyPair key = keyGen.generateKeyPair();
			File privateKeyFile = new File(privateKey);
			File publicKeyFile = new File(publicKey);
			privateKeyFile.createNewFile();
			publicKeyFile.createNewFile();
			
			ObjectOutputStream publicKeyOs = new ObjectOutputStream(new FileOutputStream(publicKeyFile));
			publicKeyOs.writeObject(key.getPublic());
			publicKeyOs.close();
			
			ObjectOutputStream privateKeyOs= new ObjectOutputStream(new FileOutputStream(privateKeyFile));
			privateKeyOs.writeObject(key.getPrivate());
			privateKeyOs.close();
			
		} catch (NoSuchAlgorithmException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static byte[] encrypt(String text, PublicKey key){
		byte[] cipherText = null;
		try{
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			cipherText = cipher.doFinal(text.getBytes());
		}catch(Exception e){
			e.printStackTrace();
		}
		return cipherText;
	}
	
	public static String decrypt(byte[] text, PrivateKey key){
		byte[] decryptedText = null;
		try{
			Cipher cipher = Cipher.getInstance("RSA");
			cipher.init(Cipher.DECRYPT_MODE, key);
			decryptedText = cipher.doFinal(text);
		}catch(Exception e){
			e.printStackTrace();
		}
		return new String(decryptedText);
	}

	public static void main(String[] args){
		String dirPrivateKey = "client_private.key";
		String dirPublicKey = "client_public.key";
		
		RSA.generateKey(dirPrivateKey, dirPublicKey);
		
		dirPrivateKey = "server_private.key";
		dirPublicKey = "server_public.key";
		
		RSA.generateKey(dirPrivateKey, dirPublicKey);

	}

}
