#!/bin/bash

set -x
set -e

killall java || true

sh createKeys.sh &

sleep 2

sh runServer.sh &

sleep 2

sh runClient.sh
