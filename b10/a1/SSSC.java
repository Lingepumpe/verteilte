//slow symetric stream cipher
//
//Works like this:
//
//Takes two parameters, key (256bit) and a nonce (64bit)
//
//!!
//Never reuse a key+nonce pair, nonce does not have to be chosen randomly,
//but needs to be different when the key is reused for each session.
//!!
//
//We generate the stream like this:
//sha3(key xor nonce xor 0) + sha3(key xor nonce xor 1) + sha3(key xor nonce xor 2)...
//
//Encryption xors the generated stream with the plain text, and returns the result
//Decryption xors the generated stream with the cipher text, and returns the result

import org.bouncycastle.jcajce.provider.digest.SHA3;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.pqc.math.linearalgebra.ByteUtils;
import java.nio.charset.Charset;

import java.math.BigInteger;
import java.util.Random;

class SSSC {
    static Random rnd = new Random(); //dont use java random number generator for real world crypto
    
    byte[] m_sessionKey;
    BigInteger counter = BigInteger.ZERO;
    int byteOffsetInOutput = 0; //since we generate output in 64 byte blocks, we need to know where in this block to start xoring for subsequent calls of processBytes
    SHA3.DigestSHA3 md = new SHA3.DigestSHA3(512);

    SSSC(byte[] key, byte[] nonce) throws DataLengthException {
        if(key.length != 256/8  || nonce.length != 64/8) {
            throw new DataLengthException("Must be 256bit key and 64bit nonce");
        }
        byte[] padding = new byte[256/8 - 64/8];
        m_sessionKey = xor(key, ByteUtils.concatenate(padding, nonce));
    }

    void processBytes(byte[] in, byte[] out) throws DataLengthException {
        if(m_sessionKey.length != 256/8 || in.length != out.length) {
            throw new DataLengthException("Must initialize and in length must be equal to out length");
        }
        int pos = 0;
        while(pos < in.length) {
            //System.out.println("processing for pos = " + pos + ", byteOffsetInOutput = " + byteOffsetInOutput);
            md.update(xor(m_sessionKey, counter.toByteArray()));
            byte[] digest = md.digest();
            digest = ByteUtils.subArray(digest, byteOffsetInOutput);
            byte[] nextBlock = xor(digest, ByteUtils.subArray(in, pos));
            int advanceBy = Math.min(digest.length, in.length - pos);
            System.arraycopy(nextBlock, 0, out, pos, advanceBy);
            pos += advanceBy;
            byteOffsetInOutput = (byteOffsetInOutput + advanceBy) % 64; //64 byte blocks
        } 
    }

    byte[] xor(byte[] a, byte[] b) {
        byte[] result = new byte[Math.max(a.length, b.length)]; //we want to keep the "too long" bytes of the longer of the two arrays
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, 0, b.length);
        
        for(int i = 0; i < Math.min(a.length, b.length); ++i) {
            result[i] = (byte)(a[i] ^ b[i]);
        }
        return result;
    }
        
    static public void main(String[] args) {


        /* generate key and nonce*/
        byte[] key = new byte[32];
        rnd.nextBytes(key);

        byte[] nonce = new byte[8]; //all zeros is fine, we will not reuse this key ever
        
        byte[] plaintext = ("The quick brown fox jumpes over the lazy dog. Why does he jump? Why does he not " +
            "dig a hole and tunnel below the dog? Because the quick brown fox might be quick, " +
            "but he is also lazy, just like the dog, and digging holes is a lot of work...").getBytes(Charset.forName("UTF-8"));

        /* encode and decode*/
        SSSC enc = new SSSC(key, nonce);
        SSSC dec = new SSSC(key, nonce);
        byte[] ciphertext = new byte[plaintext.length];
        enc.processBytes(plaintext, ciphertext);
        byte[] maybePlaintext = new byte[plaintext.length];
        dec.processBytes(ciphertext, maybePlaintext);

        /*print results*/
        System.out.println("We encrypted with key = " + ByteUtils.toHexString(key) + ", nonce = " + ByteUtils.toHexString(nonce));
        System.out.println("Plain: " + ByteUtils.toHexString(plaintext));
        System.out.println("Crypt: " + ByteUtils.toHexString(ciphertext));
        System.out.println("Plain: " + ByteUtils.toHexString(maybePlaintext));
        assert(ByteUtils.equals(plaintext, maybePlaintext));
        try {
            System.out.println("Decrypted and decoded result: " + new String(maybePlaintext, "UTF-8"));
        } catch(Exception e) {
            e.printStackTrace();
        }

        System.out.println("\nTesting on random plaintexts, encoding in random chunk sizes and decoding in other random chunk sizes...");
        for(int i = 0; i < 8192; ++i) {
            testRun();
        }
        System.out.println("Test successful");
    }

    static void testRun() {
        /* generate key and nonce*/
        byte[] key = new byte[32];
        rnd.nextBytes(key);
        
        byte[] nonce = new byte[8];
        rnd.nextBytes(nonce);
        
        byte[] plaintext = new byte[1 + rnd.nextInt(2048)];
        rnd.nextBytes(plaintext);
        
        /* create encoder and decoder*/
        SSSC enc = new SSSC(key, nonce);
        SSSC dec = new SSSC(key, nonce);
        byte[] ciphertext = new byte[plaintext.length];

        /* encode (in multiple chunk, maybe */
        int pos = 0;
        while(pos < ciphertext.length) {
            int chunk = 1 + rnd.nextInt(ciphertext.length - pos); //select a block of random length
            if(rnd.nextDouble() > 0.8) { //give a 20% chance for this to be last block
                chunk = ciphertext.length - pos;
            }
            byte[] chunkOutput = new byte[chunk];
            enc.processBytes(ByteUtils.subArray(plaintext, pos, pos + chunk), chunkOutput);
            System.arraycopy(chunkOutput, 0, ciphertext, pos, chunk);
            pos += chunk;
        }
        
        /* decode (in multiple chunks, maybe) */
        byte[] maybePlaintext = new byte[plaintext.length];
        pos = 0;
        while(pos < ciphertext.length) {
            int chunk = 1 + rnd.nextInt(ciphertext.length - pos);
            if(rnd.nextDouble() > 0.8) {
                chunk = ciphertext.length - pos;
            }
            byte[] chunkOutput = new byte[chunk];
            dec.processBytes(ByteUtils.subArray(ciphertext, pos, pos + chunk), chunkOutput);
            System.arraycopy(chunkOutput, 0, maybePlaintext, pos, chunk);
            pos += chunk;
        }

        if(!ByteUtils.equals(plaintext, maybePlaintext)) {
            throw new RuntimeException();
        }
    }
}
