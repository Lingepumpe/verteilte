#!/bin/bash

message="This is a test message"
if [ $# -gt 0 ]; then
  message=$@
fi

javac -cp bcprov-jdk15on-152.jar:. SSSC.java &&
javac -cp bcprov-jdk15on-152.jar:. Client.java &&
java -ea -cp bcprov-jdk15on-152.jar:. Client dcafb51ad00497b684b6ada5d0876a9fc4fee977d310c9c12bf254d72bec87c6 "${message}"


