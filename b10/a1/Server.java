import java.util.concurrent.*;
import java.net.*;
import java.io.*;
import java.math.*;

import javax.xml.bind.DatatypeConverter;

class Server {
    public static final int listenPort = 9191;
    public static final int threadPoolSize = 10;

    private ExecutorService service;
    boolean listening = true;

    public static void main(String[] args) {
        if(args.length != 1) {
            System.out.println("Must supply symetric key (256bits in hex) as parameter");
            System.out.println("For example: dcafb51ad00497b684b6ada5d0876a9fc4fee977d310c9c12bf254d72bec87c6");
        }
        
        byte[] key = DatatypeConverter.parseHexBinary(args[0]);
        if(key.length != 256/8) {
            System.out.println("Key must be 256bit long");
            return;
        }
        System.out.println("Hello, my name is Server");

        Server s = new Server();
        s.listenHome(key);
        System.out.println("end of server");
    }

    private void listenHome(byte[] key) {
        service = Executors.newFixedThreadPool(threadPoolSize);

        try {
            ServerSocket serverSocket = new ServerSocket(listenPort);
            while(listening) {
                try {
                    service.execute(new RequestHandler(serverSocket.accept(), key));
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        } catch(Exception e) {
            System.out.println("Exception caught");
        } finally {
            service.shutdown();
        }
    }
}

class RequestHandler implements Runnable {
    private final Socket socket;
    private boolean areAuthed = false;
    private final SSSC dec;

    RequestHandler(Socket s, byte[] key) {
        socket = s;
        byte[] nonce = new byte[8]; //we use all zeroes for nonce
        dec = new SSSC(key, nonce);
        
    }
    public void run() {
        System.out.println("New Connection");
        String inputLine;
        try (
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream())); ) {
                while( (inputLine = in.readLine()) != null) {
                    System.out.println("Received: " + inputLine);
                    byte[] ciphertext = DatatypeConverter.parseHexBinary(inputLine);
                    byte[] plaintext = new byte[ciphertext.length];
                    dec.processBytes(ciphertext, plaintext);
                    System.out.println("Decrypted and decoded: " + new String(plaintext, "UTF-8"));
                }
            } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connection closed");
    }
}
