import java.net.*;
import java.io.*;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;


class Client {
    private final SSSC enc;

    public static void main(String[] args) {
        if(args.length != 2) {
            System.out.println("First parameter must be 256bit hey key, second parameter must be message to send");
            System.out.println("For example: dcafb51ad00497b684b6ada5d0876a9fc4fee977d310c9c12bf254d72bec87c6 \"Happy is voor hobos\"");
        }
        byte[] key = DatatypeConverter.parseHexBinary(args[0]);
        if(key.length != 256/8) {
            System.out.println("Key must be 256bit long");
            return;
        }
        Client c = new Client(key);
        double r = c.oneShotRequest("localhost", args[1]);
        System.out.println("This is the end. My only friend...the end");
    }

    public Client(byte[] key) {
        byte[] nonce = new byte[8]; //we use all zeroes for nonce
        enc = new SSSC(key, nonce);
    }

    public double oneShotRequest(String hostName, String message) { //authenticates, fires one command, disconnects
        int portNumber = Server.listenPort;
        byte[] plaintext = message.getBytes(Charset.forName("UTF-8"));
        byte[] ciphertext = new byte[plaintext.length];
        enc.processBytes(plaintext, ciphertext);
        String toSend = DatatypeConverter.printHexBinary(ciphertext);
        try {
            Socket kkSocket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(kkSocket.getOutputStream(), true);
            out.println(toSend);
        } catch (UnknownHostException e) {
            System.err.println("Failed to resolve hostname " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Failed to connect to " + hostName);
            System.exit(1);
        }
        return 0;
    }
}
