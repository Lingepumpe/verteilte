
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServiceAnnouncer{

	private final int udpPort=8888;
	private final int threadPoolSize=10;
	private DatagramSocket socket;
	private DatagramPacket packet;
	private boolean listening=true;
	private ExecutorService service;
	private int tcpPort;
	private String host;
	private String identifier;

	public void announceService(String identifier, String host, int tcpPort){
		this.identifier=identifier;
		this.host=host;
		this.tcpPort=tcpPort;
		launchAnnouncer();
	}
	
	private void launchAnnouncer(){
		service=Executors.newFixedThreadPool(threadPoolSize);
		try {
			socket=new DatagramSocket(udpPort);
			packet=new DatagramPacket(new byte[1024], 1024);
			socket.setSoTimeout(10000);	//socket.receive should not block forever
			System.out.println("Waiting for client broadcast");
			while(listening){
				try{
					socket.receive(packet);
					System.out.println("Broadcast received: \""+new String(packet.getData(),0,packet.getLength())+"\"");
					service.execute(new RequestHandler(packet.getAddress(),packet.getPort(),packet.getLength(), packet.getData()));
				}catch(SocketTimeoutException e){	//handle timoutexception to check the state of listening
					if(!listening) break;
				}
			}
			
		} catch (SocketException e) {
			System.err.println("Could not construct DatagramSocket");
			System.exit(1);
		} catch (IOException e) {
		} finally{
			socket.close();
			service.shutdown();
		}
	}
	
	public void shuttingDownAnnouncer(){
		this.listening=false;
	}
	
	private class RequestHandler implements Runnable{
		
	
		private DatagramSocket socket;
		private UDPClientData udpClient;
		
		private final InetAddress address;
		private final int port;
		private final int len;
		private final byte[] data;
		
		
		RequestHandler(InetAddress address, int port, int len, byte[] data){
			this.address=address;
			this.port=port;
			this.len=len;
			this.data=data;
		}
		
		@Override
		public void run() {
			boolean requestsMyServices=ProtParse.getFirstParamAsString(new String(data,0,len)).equals(identifier);
			if(requestsMyServices){ //udpclient requests my services
				String replyPacket=buildReplyPacket();
				System.out.println("Build packet: \""+replyPacket+"\"");
				try{
					socket=new DatagramSocket();
					socket.send(new DatagramPacket(replyPacket.getBytes(), replyPacket.getBytes().length,  InetAddress.getByName(udpClient.getAddress()),udpClient.getPort()));	
				}catch(IOException e){
					System.err.println("IP of client host could not be retrieved");
					
				}finally{
					socket.close();
				}
			}
		}
		
		private String buildReplyPacket(){
			System.out.println( "Anfrage von "+address+" "+"vom Port "+port);
			
			udpClient=new UDPClientData(ProtParse.removeFirstCharacter(address.toString()), port);
			
			return ProtBuild.myService(identifier, host, Integer.toString(tcpPort)); 
		}
	 }

	 private class UDPClientData{
			private final String address;
			private final int port;
			
			private UDPClientData(String address, int port){
				this.address=address;
				this.port=port;
			}
			
			public String getAddress(){
				return address;
			}
			
			public int getPort(){
				return port;
			}
			
	}	
}

