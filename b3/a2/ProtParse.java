class ProtParse {
	public static String getCommand(String in) {
		String[] sArr = in.split("\\s+");
		return sArr[0];
	}

	public static double getFirstParamAsDouble(String in) {
		String[] sArr = in.split("\\s+");
		return Double.parseDouble(sArr[1]);
	}

	public static double getSecondParamAsDouble(String in) {
		String[] sArr = in.split("\\s+");
		return Double.parseDouble(sArr[2]);
	}

	public static int getFirstParamAsInt(String in) {
		String[] sArr = in.split("\\s+");
		return Integer.parseInt(sArr[1]);
	}

	public static String getFirstParamAsString(String in) {
		int commandLen = getCommand(in).length();
		return in.substring(commandLen).trim();
	}

	public static String getSecondParamAsString(String in) {
		return getFirstParamAsString(getFirstParamAsString(in));
	}

	public static int getThirdParamAsInt(String in) {
		String[] sArr = in.split("\\s+");
		return Integer.parseInt(sArr[3]);
	}

	/*
	 * When more then 1 parameter is available then use this method to get first
	 * parameter
	 */
	public static String getFirstParamAsStringV2(String in) {
		return getCommand(getFirstParamAsString(in));
	}

	/*
	 * When more then two parameters are available then use this method to get
	 * the second parameter
	 */
	public static String getSecondParamAsStringV2(String in) {
		return getCommand(getFirstParamAsString(getFirstParamAsString(in)));
	}

	public static String getTcpIPAsString(String in) {
		return in.split(":")[0];
	}

	public static int getTcpPortAsInt(String in) {
		return Integer.parseInt(in.split(":")[1]);
	}

	public static String removeFirstCharacter(String in){
		return in.substring(1);
	}

	public static void test() {
		assert (getCommand("add 2.5 3.777").equals("add"));
		assert (getCommand("res 0 Not authenticated").equals("res"));
		assert (getCommand("fac 77").equals("fac"));

		assert (Math.abs(getFirstParamAsDouble("add 2.5 3.777") - 2.5) < 0.0001);
		assert (Math.abs(getFirstParamAsDouble("res 0 Not authenticated") - 0) < 0.0001);
		assert (Math.abs(getFirstParamAsDouble("res 2  ") - 2) < 0.0001);
		assert (Math.abs(getFirstParamAsDouble("fac 77") - 77) < 0.0001);

		assert (Math.abs(getSecondParamAsDouble("add 2.5 3.777") - 3.777) < 0.0001);

		boolean gotException = false;
		try {
			getSecondParamAsDouble("add 2.5");
		} catch (ArrayIndexOutOfBoundsException e) {
			gotException = true;
		}
		assert (gotException);

		assert (getFirstParamAsInt("fac 77") == 77);

		assert (getFirstParamAsString("auth ingo von der wiese")
				.equals("ingo von der wiese"));

		assert (getSecondParamAsString("res 0 Authentication Failed")
				.equals("Authentication Failed"));
		assert (getSecondParamAsString("res 0").equals(""));
	}
}
