
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.net.InterfaceAddress;

public class ServiceLocator {

	private InetAddress address;
	private DatagramPacket packet;
	private DatagramPacket responsePacket=new DatagramPacket(new byte[1024], 1024);
	private DatagramSocket socket;
	
	private final int threadPoolSize = 10;
	private final int udpPort = 8888;
	private String identifier;
	private volatile boolean listening = true;
	
	private final String host=new String("127.0.0.1"); //broadcast address is 138.232.95.255, but broadcast does not work on machines in ZID
	
	private ExecutorService service;
	private ConcurrentHashMap<String, List<String>> servers = new ConcurrentHashMap<String,List<String>>();

	
	public void findServices(String identifier){
	    this.identifier=identifier;
	    launchLocator();
	}

	private void launchLocator() {
		service = Executors.newFixedThreadPool(threadPoolSize);
		
		try {
			//address = findBroadcastAddress();
			address = InetAddress.getByName(host);
			byte[] message = ProtBuild.findService(identifier).getBytes();
			packet = new DatagramPacket(message, message.length, address,
					udpPort);
			try {
				socket = new DatagramSocket();
				socket.setBroadcast(true);
				
				try {
					socket.send(packet);
					System.out.println("Broadcast sent: "+new String(packet.getData(),0,packet.getLength()));
				} catch (IOException e) {
					System.err
							.println("I/O exception occured when sendind broadcast");
					System.exit(1);
				}
			} catch (SocketException e) {
				System.err.println("Socket could not be opened");
				System.exit(1);
			}
		} catch (Exception e) {
			System.err.println("Cannot retrieve IP adress of " + host);
			System.exit(1);
		}

		try {	//after 10 seconds we assume that the servers have responded to our broadcast
			socket.setSoTimeout(10000);
		} catch (SocketException e1) {
		
		}
		
		System.out.println("Waiting 10 seconds for response");
		while (true) {
			try {
				socket.receive(responsePacket);
				service.execute(new ResponseHandler(responsePacket.getAddress(),responsePacket.getPort(),responsePacket.getLength(),responsePacket.getData()));
			} catch (IOException e) {
				System.out.println("Service Locator Timeout");
				socket.close();
				service.shutdown();
				break;
			}
		}
		
	}
	
	public InetAddress findBroadcastAddress(){
	    InetAddress broadcast=null;	    
	    try{
	      Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
	      while (interfaces.hasMoreElements()) {
		    NetworkInterface networkInterface = interfaces.nextElement();
		  
			  if (networkInterface.isLoopback()) continue;    // Don't want to broadcast to the loopback interface
			  for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
				broadcast = interfaceAddress.getBroadcast();
				if (broadcast == null)	continue;
			  }
			  
	      }
	    }catch(SocketException e){
		System.out.println("No broadcast address found");
	    }
            System.out.println("Broadcasting address detected to be: " + broadcast.getHostAddress());
	    return broadcast;
	}

	public List<String> getFoundServices(String identifier){
		return servers.get(identifier);
	}
	
	public void printAvailableServices(){
		System.out.println("Print available services");
		if(!servers.isEmpty()){
			for(String key : servers.keySet()){
				for(String value : servers.get(key)){
					System.out.println(key +": "+value);
				}
			}
		}
	}

	private class ResponseHandler implements Runnable {

		private final InetAddress address;
		private final int port;
		private final int len;
		private final byte[] data;
		
		
		ResponseHandler(InetAddress address, int port, int len, byte[] data){
			this.address=address;
			this.port=port;
			this.len=len;
			this.data=data;
		}
	
		@Override
		public void run() {
			String rep = new String(data, 0, len);
			String id = ProtParse.getFirstParamAsStringV2(rep);
			String ip = ProtParse.getSecondParamAsStringV2(rep);
			int port = ProtParse.getThirdParamAsInt(rep);
			String serverLoc = ProtBuild.addressPort(ip, port);
			
			if(servers.containsKey(id)){
				servers.get(id).add(serverLoc);
			}else{
				List<String> sList = new ArrayList<String>();
				sList.add(serverLoc);
				servers.put(id, sList);
			}

		}
	}
}
