import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

class Client {
	
	private final Random rnd = new Random();
	private final ServiceLocator locator = new ServiceLocator();
	private int port;
	private String host;

	public static void main(String[] args) {
		System.out.println("Hello, my name is Client");
		ProtParse.test();
		ProtBuild.test();

		Client c = new Client();
		c.test("Babylon");

		System.out.println("This is the end. My only friend...the end");
	}

	public void test(String service) {
		locator.findServices(service);
		if (checkService(service)) {
			chooseServerRandomly(service);
			double r = oneShotRequest(host, "admin", ProtBuild.fac(99));
			System.out.println("Result for fac(99) is: " + r);
			r = oneShotRequest(host, "admin", ProtBuild.add(-33, 5.5));
			System.out.println("Result for add(-33, 5.5) is: " + r);
			randomRequests(host, "admin");
		} else {
			System.out.println("No server with desired service found");
		}
	}

	public boolean checkService(String service) {
		return locator.getFoundServices(service) != null;
	}
	
	public void chooseServerRandomly(String service){
		int random = rnd.nextInt(locator.getFoundServices(service).size());
		String address = locator.getFoundServices(service).get(random);
		host=ProtParse.getTcpIPAsString(address);
		port=ProtParse.getTcpPortAsInt(address);
	}
	
	public void randomRequests(String hostName, String authname) { // authenticates, fires multiple, random commands, with random sleeps in between, disconnects
																	
		int portNumber = port;
		
		try (Socket kkSocket = new Socket(hostName, portNumber);
				PrintWriter out = new PrintWriter(kkSocket.getOutputStream(),
						true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						kkSocket.getInputStream()));) {
			String fromServer;

			out.println(ProtBuild.auth(authname));
			fromServer = in.readLine();
			if (fromServer != null) {
				String err = ProtParse.getSecondParamAsString(fromServer);
				if (!err.equals("")) {
					System.out.println("Server responded with error: " + err);
					return;
				}
			}
			while (true) {
				try {
					Thread.sleep(rnd.nextInt(500));
				} catch (Exception e) {
					e.printStackTrace();
					return;
				}
				String operation;

				if (rnd.nextInt(200) == 0) { // generate shutdown command: only with low chance
					operation = ProtBuild.shutdown();
				} else {
					switch (rnd.nextInt(4)) {
					case 0:
						operation = ProtBuild.add(2000 * (rnd.nextDouble() - 0.5),2000 * (rnd.nextDouble() - 0.5));
						break;
					case 1:
						operation = ProtBuild.sub(2000 * (rnd.nextDouble() - 0.5),2000 * (rnd.nextDouble() - 0.5));
						break;
					case 2:
						operation = ProtBuild.mul(2000 * (rnd.nextDouble() - 0.5),2000 * (rnd.nextDouble() - 0.5));
						break;
					case 3:
						operation = ProtBuild.fac(rnd.nextInt(101));
						break;
					default:
						throw new IllegalArgumentException(
								"nextInt behaving unexpectedly");
					}
				}
				out.println(operation);
				fromServer = in.readLine();
				if (fromServer == null) {
					break;
				}
				double r = ProtParse.getFirstParamAsDouble(fromServer);
				String err = ProtParse.getSecondParamAsString(fromServer);
				if (!err.equals("")) {
					System.out.println("Server responded with error: " + err);
					return;
				}
				System.out.println("Operation \"" + operation + "\" yields "+ r);
			}
		} catch (UnknownHostException e) {
			System.err.println("Failed to resolve hostname " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Failed to connect to " + hostName);
			System.exit(1);
		}
	}

	public double oneShotRequest(String hostName, String authname, String operation) { // authenticates, fires one command, disconnects
		
		int portNumber = port;
		try (Socket kkSocket = new Socket(hostName, portNumber);
				PrintWriter out = new PrintWriter(kkSocket.getOutputStream(),
						true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						kkSocket.getInputStream()));) {
			String fromServer;

			out.println(ProtBuild.auth(authname));
			fromServer = in.readLine();
			if (fromServer != null) {
				String err = ProtParse.getSecondParamAsString(fromServer);
				if (!err.equals("")) {
					System.out.println("Server responded with error: " + err);
					return 0;
				}
			}
			out.println(operation);
			fromServer = in.readLine();
			if (fromServer != null) {
				double r = ProtParse.getFirstParamAsDouble(fromServer);
				String err = ProtParse.getSecondParamAsString(fromServer);
				if (!err.equals("")) {
					System.out.println("Server responded with error: " + err);
					return 0;
				}
				return r;
			}
		} catch (UnknownHostException e) {
			System.err.println("Failed to resolve hostname " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Failed to connect to " + hostName);
			System.exit(1);
		}
		return 0;
	}
}
