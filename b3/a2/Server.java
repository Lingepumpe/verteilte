import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class Server {

	
	private Thread serviceAnnouncerThread = null;
	private final int listenPort = 9191;
	private final int threadPoolSize = 10;
	private ExecutorService service;
	private volatile boolean listening = true;
	private ServerSocket serverSocket = null;
	private ServiceAnnouncer serviceAnnouncer = new ServiceAnnouncer();

	public static void main(String[] args) {

		System.out.println("Hello, my name is Server");
		ProtParse.test();
		ProtBuild.test();

		Server s = new Server();
		s.startServiceAnnouncer("Babylon","localhost",s.getListenPort());

		try {
			Runtime.getRuntime().addShutdownHook(s.handlerInterrupt());	//catch Ctrl+C
		} catch (Throwable t) {
			System.out.println("Could not add Shutdown hook");
		}

		s.listenHome();
		System.out.println("end of server");
	}

	public int getListenPort(){
	    return listenPort;
	}

	public void startServiceAnnouncer(String identifier, String host, int tcpPort) {
		final String temp_identifier=identifier;
		final String temp_host=host;
		final int  temp_tcpPort=tcpPort;
		serviceAnnouncerThread = new Thread(new Runnable() {

			@Override
			public void run() {
				serviceAnnouncer.announceService(temp_identifier, temp_host, temp_tcpPort);
			}

		});
		serviceAnnouncerThread.start();
	}

	public Thread handlerInterrupt() {
		return new Thread() {
			@Override
			public void run() {
				requestShutdown();
			}
		};
	}

	public void requestShutdown() {
		listening = false; // everything else happens in the main thread, boolean is volatile to make sure the change propagates
		serviceAnnouncer.shuttingDownAnnouncer();
		serviceAnnouncerThread.interrupt();
		try {
			serviceAnnouncerThread.join();
		} catch (InterruptedException e) {
			// ignore
		}
	}

	public boolean areShuttingDown() {
		return !listening;
	}

	private void listenHome() {
		service = Executors.newFixedThreadPool(threadPoolSize);

		try {
			serverSocket = new ServerSocket(listenPort);
			serverSocket.setSoTimeout(500); 
			// make sure serverSocket.accept() doesn't block forever...to be able to notice a shutdown in this (the main) thread
			while (listening) {
				try {
					service.execute(new RequestHandler(serverSocket.accept(),
							this));
				} catch (SocketTimeoutException e) {
					// ignore the exception
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("Exception caught");
		} finally {
			try {
				serverSocket.close();
			} catch (Exception e) {
			}
			service.shutdown();
			System.out.println("Main Server Thread terminating");
		}
	}
}

class RequestHandler implements Runnable {
	private final Socket socket;
	private final Server server;
	private boolean areAuthed = false;

	RequestHandler(Socket sock, Server srv) {
		socket = sock;

		server = srv;
	}

	@Override
	public void run() {
		System.out.println("New Connection");
		String inputLine;
		try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));) {
			socket.setSoTimeout(500);
			while (true) {
				try {
					if (server.areShuttingDown()
							|| (inputLine = in.readLine()) == null) {
						break;
					}
					System.out.println("Received: " + inputLine);
					out.println(generateResponse(inputLine));
				} catch (SocketTimeoutException e) {
					// ignore
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			socket.close();
		} catch (Exception e) {
			// ignore
		}
		System.out.println("Connection closed");
	}

	private String generateResponse(String inputLine) {
		String command = ProtParse.getCommand(inputLine);
		if (!areAuthed) {
			if (command.equals("auth")) {
				String authName = ProtParse.getFirstParamAsString(inputLine);
				if (authName.equals("admin") || authName.equals("yakup")
						|| authName.equals("yusuf") || authName.equals("peter")) {
					areAuthed = true;
					return ProtBuild.res(0);
				}
				return ProtBuild.res(0, "Invalid credentials");
			}
			return ProtBuild.res(0, "Authentication required");
		}

		if (command.equals("add") || command.equals("sub")
				|| command.equals("mul")) {
			double x, y;
			try {
				x = ProtParse.getFirstParamAsDouble(inputLine);
				y = ProtParse.getSecondParamAsDouble(inputLine);
			} catch (Exception e) {
				return ProtBuild.res(0,
						"Invalid parameters...expected two doubles");
			}
			if (command.equals("add")) {
				return ProtBuild.res(x + y);
			} else if (command.equals("sub")) {
				return ProtBuild.res(x - y);
			} else if (command.equals("mul")) {
				return ProtBuild.res(x * y);
			}
		} else if (command.equals("fac")) {
			int n;
			try {
				n = ProtParse.getFirstParamAsInt(inputLine);
			} catch (Exception e) {
				return ProtBuild.res(0, "Invalid parameters, expected one int");
			}
			if (n < 0) {
				return ProtBuild.res(0, "Parameter to fac must be positive");
			}
			return ProtBuild.res(factorial(n).doubleValue());
		} else if (command.equals("shutdown")) {
			System.out.println("Server shutdown requested");
			server.requestShutdown();
			return ProtBuild.res(0);
		}
		return ProtBuild.res(0, "Unknown Command");
	}

	static BigInteger recfact(long start, long n) {
		long i;
		if (n <= 16) {
			BigInteger r = BigInteger.valueOf(start);
			for (i = start + 1; i < start + n; i++) {
				r = r.multiply(BigInteger.valueOf(i));
			}
			return r;
		}
		i = n / 2;
		return recfact(start, i).multiply(recfact(start + i, n - i));
	}

	static BigInteger factorial(long n) {
		return recfact(1, n);
	}
}
