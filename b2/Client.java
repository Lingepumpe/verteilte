import java.net.*;
import java.io.*;


class Client {
    public static void main(String[] args) {
        System.out.println("Hello, my name is Client");
        ProtParse.test();
        ProtBuild.test();

        Client c = new Client();
        double r = c.oneShotRequest("localhost", "admin", ProtBuild.fac(99));
        System.out.println("Result for fac(99) is: " + r);

        r = c.oneShotRequest("localhost", "admin", ProtBuild.add(-33, 5.5));
        System.out.println("Result for add(-33, 5.5) is: " + r);

        System.out.println("This is the end. My only friend...the end");
    }

    public double oneShotRequest(String hostName, String authname, String operation) { //authenticates, fires one command, disconnects
        int portNumber = Server.listenPort;

        try (
            Socket kkSocket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(kkSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(kkSocket.getInputStream())); ) {
                String fromServer;
                String fromUser;

                out.println(ProtBuild.auth(authname));
                fromServer = in.readLine();
                if(fromServer != null) {
                    String err = ProtParse.getSecondParamAsString(fromServer);
                    if(! err.equals("")) {
                        System.out.println("Server responded with error: " + err);
                        return 0;
                    }
                }
                out.println(operation);
                fromServer = in.readLine();
                if(fromServer != null) {
                    double r = ProtParse.getFirstParamAsDouble(fromServer);
                    String err = ProtParse.getSecondParamAsString(fromServer);
                    if(! err.equals("")) {
                        System.out.println("Server responded with error: " + err);
                        return 0;
                    }
                    return r;
                }
            }
        catch (UnknownHostException e) {
            System.err.println("Failed to resolve hostname " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Failed to connect to " + hostName);
            System.exit(1);
        }
        return 0;
    }
}
