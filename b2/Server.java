import java.util.concurrent.*;
import java.net.*;
import java.io.*;
import java.math.*;

class Server {
    public static final int listenPort = 9191;
    public static final int threadPoolSize = 10;

    private ExecutorService service;
    boolean listening = true;

    public static void main(String[] args) {
        System.out.println("Hello, my name is Server");
        ProtParse.test();
        ProtBuild.test();

        Server s = new Server();
        s.listenHome();
        System.out.println("end of server");
    }

    private void listenHome() {
        service = Executors.newFixedThreadPool(threadPoolSize);

        try {
            ServerSocket serverSocket = new ServerSocket(listenPort);
            while(listening) {
                try {
                    service.execute(new RequestHandler(serverSocket.accept()));
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        } catch(Exception e) {
            System.out.println("Exception caught");
        } finally {
            service.shutdown();
        }
    }
}

class RequestHandler implements Runnable {
    private final Socket socket;
    private boolean areAuthed = false;

    RequestHandler(Socket s) {
        socket = s;
    }
    public void run() {
        System.out.println("New Connection");
        String inputLine;
        try (
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream())); ) {
                while( (inputLine = in.readLine()) != null) {
                    System.out.println("Received: " + inputLine);
                    out.println(generateResponse(inputLine));
                }
            } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connection closed");
    }

    private String generateResponse(String inputLine) {
        String command = ProtParse.getCommand(inputLine);
        if(!areAuthed) {
            if(command.equals("auth")) {
                String authName = ProtParse.getFirstParamAsString(inputLine);
                if(authName.equals("admin") || authName.equals("yakup") || authName.equals("yusuf") || authName.equals("peter")) {
                    areAuthed = true;
                    return ProtBuild.res(0);
                }
                return ProtBuild.res(0, "Invalid credentials");
            }
            return ProtBuild.res(0, "Authentication required");
        }
        
        if(command.equals("add") || command.equals("sub") || command.equals("mul")) {
            double x, y;
            try {
                x = ProtParse.getFirstParamAsDouble(inputLine);
                y = ProtParse.getSecondParamAsDouble(inputLine);
            } catch(Exception e) {
                return ProtBuild.res(0, "Invalid parameters...expected two doubles");
            }
            if(command.equals("add")) {
                return ProtBuild.res(x + y);
            } else if(command.equals("sub")) {
                return ProtBuild.res(x - y);
            } else if(command.equals("mul")) {
                return ProtBuild.res(x * y);
            }
        } else if(command.equals("fac")) {
            int n;
            try {
                n = ProtParse.getFirstParamAsInt(inputLine);
            } catch(Exception e) {
                return ProtBuild.res(0, "Invalid parameters, expected one int");
            }
            if(n < 0) {
                return ProtBuild.res(0, "Parameter to fac must be positive");
            }
            return ProtBuild.res(factorial(n).doubleValue());
        }
        return ProtBuild.res(0, "Unknown Command");
    }


    static BigInteger recfact(long start, long n) {
        long i;
        if (n <= 16) { 
            BigInteger r = BigInteger.valueOf(start);
            for (i = start + 1; i < start + n; i++) {
                r = r.multiply(BigInteger.valueOf(i));
            }
            return r;
        }
        i = n / 2;
        return recfact(start, i).multiply(recfact(start + i, n - i));
    }

    static BigInteger factorial(long n) {
        return recfact(1, n);
    }
}
