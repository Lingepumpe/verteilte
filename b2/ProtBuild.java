class ProtBuild {
    public static String add(double x, double y) {
        return "add " + twoDoubleParams(x, y);
    }

    public static String sub(double x, double y) {
        return "sub " + twoDoubleParams(x, y);
    }

    public static String mul(double x, double y) {
        return "mul " + twoDoubleParams(x, y);
    }

    public static String fac(int n) {
        return "fac " + Integer.toString(n);
    }

    public static String auth(String name) {
        return "auth " + name;
    }

    public static String res(double r) {
        return res(r, "");
    }

    public static String res(double r, String err) {
        return "res " + Double.toString(r) + " " + err;
    }

    public static void test() {
        String out = add(2.5, 0);
        assert(ProtParse.getCommand(out).equals("add"));
        assert(Math.abs(ProtParse.getFirstParamAsDouble(out) - 2.5) < 0.0001);
        assert(Math.abs(ProtParse.getSecondParamAsDouble(out) - 0) < 0.0001);
    }

    private static String twoDoubleParams(double x, double y) {
        return Double.toString(x) + " " + Double.toString(y);
    }

}
